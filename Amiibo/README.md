Amiibos Faltantes [ ** Compra en camino]
=================

## Smash

	* Mario **
	* Yoshi	**
	* Corrin Player 2


## Zelda **(Completa)**

## Splatoon **(Completa)**

## Mario Maker 
	* Mario 8-bit Modern Colors

## Metroid **(Completa)**

## Chibi Robo **(Completa)**


	
Amiibos en la colección
========================

## Smash

	* Peach
	* Donkey Kong
	* Link
	* Fox
	* Samus
	* Wii Fit Trainer
	* Villager
	* Pikachu
	* Kirby
	* Marth
	* Zelda
	* Diddy Jong
	* Luigi
	* Little Mac
	* Pit
	* Captain Falcon
	* Rosalina
	* Bowser
	* Lucario
	* Toon Link
	* Sheik
	* Ike
	* Shulk
	* Sonic
	* Maga Man
	* King Dedede
	* Meta Knight
	* Robin
	* Lucina
	* Wario
	* Charizard
	* Ness
	* Pacman
	* Greninja
	* Jigglypuff
	* Palutena
	* Dark Pit
	* Zero Suit Samus
	* Ganondorf
	* Dr. Mario
	* Bowser Jr.
	* Pikmin & Olimar
	* Mr. Game & Watch
	* R.O.B.
	* Duck Hunt
	* Mii Brawler
	* Mii Swordfighter
	* Mii Gunner
	* Mewtwo
	* Falco
	* Lucas
	* R.O.B. (Japan)
	* Roy
	* Ryu
	* Bayonetta
	* Bayonetta Player 2
	* Cloud
	* Ridley
	* Wolf
	* Inkling
	* King K. Rool
	* Ice Climbers
	* Pirana Plant
	* Pichu
	* Ken
	* Daisy
	* Young Link 
	* Isabelle
	* Ivysaur
	* Pokémon Trainer
	* Squirtle
	* Simon
	* Snake
	* Chrom
	* Incineroar
	* Richter 
	* Dark Samus
	* Corrin
	* Cloud Player 2

## Super Mario

	* Peach 
	* Toad
	* Bowser (Wedding Outfit)
	* Mario (Wedding Outfit)
	* Peach (Wedding Outfit)

## Zelda 
	
	* Wolf Link
	* Link (rider)
	* Link (archer)
	* Guardian
	* Link (Ocarina of Time)
	* Link (The Legend of Zelda)
	* Toon Link (The Wind Waker)
	* Zelda (The Wind Waker)
	* Zelda
	* Link (Majora's Mask)
	* Link (Skyword Sword)
	* Link (Twilight Princess)
	* Daruk (Goron Champion)
	* Revali (Rito Champion)
	* Mipha (Zora Champion)
	* Urbosa (Gerudo Champion)
	* Link (Link's Awakening)
	* Bokoblin

## Splatoon **(Completa)**

	* Inkling Girl
	* Inkling Boy
	* Inkling Squid [Green]
	* Inkling Girl v2
	* Inkling Boy v2
	* Inkling Squid v2 [Orange]
	* Callie
	* Marie
	* Inkling Girl v3
	* Inkling Boy v3
	* Inkling Squid v3 [Purple]
	* Marina
	* Pearl
	* Octoling Girl
	* Octoling Boy
	* Octoling Octopus

## Mario Maker

	* Mario 8-bit Classic Colors

## Metroid **(Completa)**

	* Samus Aran
	* Metroid

## Yoshi's Wooly World

	* Green Yarn Yoshi

## Chibi Robo **(Completa)**
	
	* Chibi Robo

## Cards

	* Shadow Mewtwo

## Others

	* Detective Pikachu












